﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace Innlevering2_opg1
{
    public class Customer
    {
        public String Name;
        public CookieShop FavoriteCookieShop;
        public double Delay = 1000;
        private int boostDelay = 800;
        private Stopwatch sw = new Stopwatch();

        public Customer(String name, CookieShop fb)
        {
            this.Name = name;
            this.FavoriteCookieShop = fb;
        }

        public void EatMoreCookiesInALoop()
        {
            sw.Start();
            Random rnd = new Random();
            Console.ForegroundColor = ConsoleColor.White;

            while (true)
            {
                // Sjekker om det har gått ett sekund.
                if (sw.Elapsed.Milliseconds > boostDelay)
                {
                    // Hvis ja, test om kjøperen får en boost.
                    sw.Restart();
                    int random = rnd.Next(0, 100);
                    if(random < 50)
                        Delay *= 0.75;

                }
                bool gotcookie = FavoriteCookieShop.BuyCookie(this);

                if (gotcookie)
                    Delay += rnd.Next(5, 16); // Øk delay for å simulere utmattelse


                Thread.Sleep((int)Delay);
            }
        }
    }
}
