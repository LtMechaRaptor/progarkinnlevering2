﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Innlevering2_opg1
{
    public class Bakery
    {
        private CookieShop shop;
        private int Delay = 667;
        private String name;

        public Bakery(CookieShop shop, String name)
        {
            this.shop = shop;
            this.name = name;
        }

        public void CookieBakerLoop()
        {
            while (shop.CurrentBakedCookies < shop.DailyQuota)
            {
                int cookieId = shop.NewCookie();
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(name + " baked cookie " + cookieId);
                Thread.Sleep(Delay);
            }   
        }

    }
}
