﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Innlevering2_opg1
{
    public class CookieShop
    {
        private Queue<Cookie> cookies = new Queue<Cookie>();
        public int DailyQuota { get { return 100; }}
        public int CurrentBakedCookies { get; private set; }

        private static readonly object MTLock = new object();

        private Stopwatch sw = new Stopwatch();
        private TimeSpan ts = new TimeSpan();
        public CookieShop()
        {
            sw.Start();
            CurrentBakedCookies = 0;
        }

        public int NewCookie()
        {
            int cookieId = ++CurrentBakedCookies;
            cookies.Enqueue(new Cookie(cookieId));
            return cookieId;
        }
        public bool BuyCookie(Customer customer)
        {
        if (cookies.Count > 0)
            {
                lock (MTLock)
                {
                    if (cookies.Count > 0)
                    {

                        var cookie = cookies.Dequeue();
                        Console.ForegroundColor = ConsoleColor.Green;
                        if (cookie != null)
                            Console.WriteLine("Cookie " + cookie.ID + " sold to customer: " + customer.Name);
                        return true;
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("No more cookies at the moment, sorry " + customer.Name);
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }
            
        }
    }
}
