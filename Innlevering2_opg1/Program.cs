﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Innlevering2_opg1
{
    class Program
    {
        static void Main(string[] args)
        {
            CookieShop MamasHomebakedCookies = new CookieShop();

            Bakery bakery1 = new Bakery(MamasHomebakedCookies, "bakery1");
            Bakery bakery2 = new Bakery(MamasHomebakedCookies, "bakery2");

            Customer FatJones = new Customer("Fat Jones", MamasHomebakedCookies);
            Customer Bob = new Customer("Bob Duch", MamasHomebakedCookies);
            Customer Pepper = new Customer("Pepper Potts", MamasHomebakedCookies);

            Thread bakery1Thread = new Thread(new ThreadStart(bakery1.CookieBakerLoop));
            Thread bakery2Thread = new Thread(new ThreadStart(bakery2.CookieBakerLoop));

            Thread fatJonesThread = new Thread(new ThreadStart(FatJones.EatMoreCookiesInALoop));
            Thread bobThread = new Thread(new ThreadStart(Bob.EatMoreCookiesInALoop));
            Thread pepperThread = new Thread(new ThreadStart(Pepper.EatMoreCookiesInALoop));

            // Start threads
            bakery1Thread.Start();
            bakery2Thread.Start();
            fatJonesThread.Start();
            bobThread.Start();
            pepperThread.Start();

            // Wait for threads and then quit
            bakery1Thread.Join();
            bakery2Thread.Join();

            Thread.Sleep(1000);

            fatJonesThread.Abort();
            bobThread.Abort();
            pepperThread.Abort();

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Thank you for shopping. Please come again!");
        }
    }
}
